package com.garranto;

public class PaymentProcessingFactory {

    public PaypalGateway getPaypalGateway(){
        return new PaypalGateway();
    }

    public StripeGateway getStripeGateway(){
        return new StripeGateway();
    }

    public PaymentProcessing getPaymentProcessing(GatewaySpecifications gateway){
        return new PaymentProcessing(gateway);
    }
}


//spring ioc / spring core
//beans