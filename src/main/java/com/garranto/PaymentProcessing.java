package com.garranto;

public class PaymentProcessing {

    GatewaySpecifications gateway;

    public PaymentProcessing(GatewaySpecifications gateway){
        this.gateway = gateway;
    }
    public void makePayment(double amount){
        gateway.payment(amount);
    }

    public void checkTransactionStatus(int id){

        String status = gateway.findTransactionStatus(id);
        System.out.println(status);
    }
}

//tight coupled or loosely coupled

//abstract reference

//specifiications --  abstraction --  interface
