package com.garranto;

public class DependencyInjectionDempApp {
    public static void main(String[] args) {
        System.out.println("Depedency Injection Demo");
        System.out.println("=============================");

        PaymentProcessingFactory factory = new PaymentProcessingFactory();

        PaypalGateway paypal = factory.getPaypalGateway();
        StripeGateway stripe = factory.getStripeGateway();
        PaymentProcessing processing = factory.getPaymentProcessing(stripe);
        processing.makePayment(1000);

    }
}

//control the application work flow
//creating and managing object


//payment processing --

//payment gateway

//tightly or loosely

//inversion of control

//di --factory design pattern

