package com.garranto;

public interface GatewaySpecifications {
    void payment(double amount);
    String findTransactionStatus(int id);
}

//you dont see how things are getting implementing
//you will only see what are/to be implemented
